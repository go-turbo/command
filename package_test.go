// Copyright (c) 2018-2019 The Go-Turbo Project
// Copyright (c) 2016-2017 Mathias J. Hennig
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package command

// Standard packages are documented at https://golang.org/pkg/.
import (
	"flag"
	"fmt"
	"os"
)

// ExampleWriteOptions demonstrates how to use function WriteOptions.
func ExampleWriteOptions() {

	flags := &flag.FlagSet{}
	flags.Bool("debug", false, "Enable debug mode.")
	flags.Bool("v", false, "Enable verbose output.")

	// Note that Flag instances (https://golang.org/pkg/flag/#Flag) with
	// a DefValue of "true" are suffixed with "=false" in the output!
	flags.Bool("log", true, "Disable logging.")

	// https://golang.org/pkg/os/#Chdir
	helpChdir := "Switch to a different working directory before execution."
	flags.Var(Option(os.Chdir), "C", helpChdir)

	WriteOptions(os.Stdout, flags)
	// Output:
	// Options:
	//     -C ...
	//         Switch to a different working directory before execution.
	//     --debug
	//         Enable debug mode.
	//     --log=false
	//         Disable logging.
	//     -v
	//         Enable verbose output.
	//     --help, -h
	//         Print this message and exit gracefully.
	//
}

// ExampleWriteUsage demonstrates how to use function WriteUsage.
func ExampleWriteUsage() {

	WriteUsage(os.Stdout, "example", "[-h] command ...", "--help")
	// Output:
	// Usage:  example [-h] command ...
	//         example --help
	//
}

// ExampleOption demonstrates how to use type Option.
func ExampleOption() {

	handler := func(value string) error {
		_, err := fmt.Println(value)
		return err
	}

	flags := &flag.FlagSet{}
	flags.Var(Option(os.Chdir), "chdir", "Switch working directory.")
	flags.Var(Option(handler), "print", "Print the given option.")

	flags.Parse([]string{"--print", "Hello World!"})
	// Output:
	// Hello World!
}
