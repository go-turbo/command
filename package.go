// Copyright (c) 2018-2019 The Go-Turbo Project
// Copyright (c) 2016-2017 Mathias J. Hennig
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Package command provides utilities for CLI applications.
package command

// Standard packages are documented at https://golang.org/pkg/.
import (
	"flag"
	"fmt"
	"io"
)

// WriteOptions generates the part of a CLI executable's `--help` message that
// lists and briefly explains the flags or options available to the user.
// The output format (see examples) is an attempt to align the behavior of Go's
// native package `flag` with the expectations of POSIX system users.
func WriteOptions(output io.Writer, flags *flag.FlagSet) error {

	handler := func(item *flag.Flag) {

		if len(item.Name) == 1 {
			fmt.Fprint(output, "    -")
		} else {
			fmt.Fprint(output, "    --")
		}

		if item.DefValue == "false" {
			fmt.Fprintln(output, item.Name)
		} else if item.DefValue == "true" {
			fmt.Fprintln(output, item.Name+"=false")
		} else {
			fmt.Fprintln(output, item.Name, "...")
		}

		fmt.Fprintln(output, "       ", item.Usage)
	}

	fmt.Fprintln(output, "Options:")
	flags.VisitAll(handler)

	fmt.Fprintln(output, "    --help, -h")
	fmt.Fprintln(output, "        Print this message and exit gracefully.")

	_, err := fmt.Fprintln(output)
	return err
}

// WriteUsage generates the part of a CLI executable's `--help` message that
// provides a brief overview of the invocation scheme, based on the given name
// and examples.
func WriteUsage(output io.Writer, name string, examples ...string) error {

	fmt.Fprintln(output, "Usage: ", name, examples[0])

	for _, item := range examples[1:] {
		fmt.Fprintln(output, "       ", name, item)
	}

	_, err := fmt.Fprintln(output)
	return err
}

// Option implements the flag.Value interface based on the signature of
// method Set. In combination with method FlagSet.Var or function flag.Var,
// it allows for defining custom flags by decorating closures, lexical mutators
// and the like. Refer to https://golang.org/pkg/flag/ for more information.
type (
	Option func(value string) error
)

// Set invokes the underlying function, passing through the given string
// and returned error value. Invocation on a nil pointer is not an error.
func (flag Option) Set(value string) (err error) {

	if flag != nil {
		err = flag(value)
	}

	return err
}

// String always returns "...".
func (flag Option) String() string {

	return "..."
}
